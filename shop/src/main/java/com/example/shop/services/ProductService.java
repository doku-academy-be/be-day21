package com.example.shop.services;

import com.example.shop.dto.CreateProductRequest;
import com.example.shop.dto.ProductResponse;
import com.example.shop.entities.Products;
import com.example.shop.exception.*;
import com.example.shop.repositories.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private ProductRepository productRepository;

    private Products convertToEntity(CreateProductRequest req) {
        return modelMapper.map(req, Products.class);
    }

    private ProductResponse convertToDto(Products user) {
        return modelMapper.map(user, ProductResponse.class);
    }



    public ProductResponse createProduct(CreateProductRequest product) {

        if(productRepository.existsByName(product.getName()))
        {
            throw new AlreadyExistException("Data: "+product.getName()+" already exist");
        }

        try{
             if(product.getDescription().length()==200)
             {
                     throw new StringTooLongException("Maximum 200 character ");
             }
        } catch (StringTooLongException e) {}

        if (product.getName().isEmpty() || product.getDescription().isEmpty()){
            throw new NullPointerException("Data cannot be null");
        }

        try {
           Integer.parseInt(product.getStock());
        } catch (NumberFormatException e){
            throw new NumberFormatException("Data must be integer");
        }

        if(product.getPrice() < 0){
            throw new NegativeNumberException("Data price cannot be negative number");

        }


        Products prd = convertToEntity(product);
        Products create = productRepository.save(prd);
        return convertToDto(create);

    }



    public List<ProductResponse> getProductList() {
        List<Products> prod = productRepository.findAll();
        if (!prod.isEmpty()) {
            return prod.stream()
                    .map(products -> modelMapper.map(products, ProductResponse.class))
                    .collect(Collectors.toList());
        }
        throw new NotFoundException("Data product is empty");

    }

    public ProductResponse getProductById(Long id) {
//        Optional<Products> getprod = productRepository.findById(id);
//        if (getprod.isPresent()){
//            Products prod = getprod.get();
//            return convertToDto(prod) ;
//        }else {
//            throw new NotFoundException("Not Found");
//        }
        Products prod = productRepository.findById(id).orElseThrow(()->new NotFoundException("User ID: "+ id +" not found "));
        return convertToDto(prod) ;
    }

    public ProductResponse updateProductById(Products product) {
        Optional<Products> productFound = productRepository.findById(product.getId());

        if (productFound.isPresent()) {
            Products userUpdate = productFound.get();
            userUpdate.setName(product.getName());
            userUpdate.setPrice(product.getPrice());
            userUpdate.setStock(product.getStock());
            Products prd = productRepository.save(product);
            return convertToDto(prd);
        } else {
            throw new NotFoundException("Data product: "+product.getId()+" not found");
        }
    }

    public String deleteProductById(Long id) {
        productRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}
