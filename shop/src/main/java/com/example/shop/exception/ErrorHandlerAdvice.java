package com.example.shop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandlerAdvice {
    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<Object> handlerGeneralError(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1500","General error, internal server error"),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> handlerNotFound(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1404","Data not found"),
                HttpStatus.NOT_FOUND

        );
    }

    @ExceptionHandler(value = NullPointerException.class)
    public ResponseEntity<Object> handlerNotNull(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1400","Cannot be null"),
                HttpStatus.BAD_REQUEST
        );
    }
    @ExceptionHandler(value = AlreadyExistException.class)
    public ResponseEntity<Object> handlerAlreadyExist(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1410","Data already exist"),
                HttpStatus.BAD_REQUEST
        );
    }
    @ExceptionHandler(value = NumberFormatException.class)
    public ResponseEntity<Object> handlerNumberFormat(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1420","Data must be numeric"),
                HttpStatus.BAD_REQUEST
        );
    }
    @ExceptionHandler(value = NegativeNumberException.class)
    public ResponseEntity<Object> handlerNegativeNumber(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1430","Data cannot be a negative number"),
                HttpStatus.BAD_REQUEST
        );
    }
    @ExceptionHandler(value = StringTooLongException.class)
    public ResponseEntity<Object> handlerStringTooLong(RuntimeException e){
        return new ResponseEntity<>(
                buildResponse("1440","Data is longer"),
                HttpStatus.BAD_REQUEST
        );
    }

    private Map<String,String> buildResponse(String code, String message){
        Map<String, String> response = new HashMap<>();
        response.put("code", code);
        response.put("message", message);
        return response;
    }
}

