package com.example.shop.exception;

public class NegativeNumberException extends RuntimeException {
    public NegativeNumberException(String message){
        super(message);
    }
}
