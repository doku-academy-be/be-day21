package com.example.shop.controller;

import com.example.shop.dto.CreateProductRequest;
import com.example.shop.dto.ProductResponse;
import com.example.shop.entities.Products;
import com.example.shop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private ProductService services;


    @GetMapping
    public ResponseEntity<List<ProductResponse>> getAllProducts() {
        return ResponseEntity.ok(services.getProductList());
    }


    @GetMapping("/{id}")
    public ResponseEntity<ProductResponse> getProductById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getProductById(id));
    }


    @PostMapping
    public ResponseEntity<ProductResponse> addUser(@RequestBody CreateProductRequest product) {
        return ResponseEntity.ok(this.services.createProduct(product));
    }


    @PutMapping
    public ResponseEntity<ProductResponse> updateProduct(@RequestBody Products product) {
        return ResponseEntity.ok().body(this.services.updateProductById(product));
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteProduct(@PathVariable Long id) {
        this.services.deleteProductById(id);
        return HttpStatus.OK;
    }
}
