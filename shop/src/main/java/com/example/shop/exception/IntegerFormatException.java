package com.example.shop.exception;

public class IntegerFormatException extends RuntimeException{
    public IntegerFormatException(String message){
        super(message);
    }
}
