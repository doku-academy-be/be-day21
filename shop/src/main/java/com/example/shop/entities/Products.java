package com.example.shop.entities;


import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;





@Entity
@Table(name="products")
@Getter
@Setter
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @Column(unique = true)
    private String name;
//    @Column(length = 200)
    private String description;
     private double price;
    private int stock;



}
