package com.example.shop.exception;

public class StringTooLongException extends RuntimeException{
    public StringTooLongException(String message){
        super(message);
    }
}
